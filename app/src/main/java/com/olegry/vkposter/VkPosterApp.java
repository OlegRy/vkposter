package com.olegry.vkposter;

import android.app.Application;
import android.content.Intent;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

public class VkPosterApp extends Application {

    VKAccessTokenTracker mVKAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken vkAccessToken, VKAccessToken vkAccessToken1) {
            if (vkAccessToken1 == null) {
                Intent intent = new Intent(VkPosterApp.this, VkPosterActivity.class);
                startActivity(intent);
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mVKAccessTokenTracker.startTracking();
        VKSdk.initialize(this);
    }
}
