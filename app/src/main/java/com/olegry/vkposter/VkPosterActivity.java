package com.olegry.vkposter;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.olegry.vkposter.fragments.VkPosterFragment;
import com.olegry.vkposter.utils.Utils;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

public class VkPosterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vk_poster);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (VKSdk.wakeUpSession(getApplicationContext())) {
            showFragment();
        } else {
            VKSdk.login(this, Utils.SCOPES);
        }
    }

    private void showFragment() {
        if (getSupportFragmentManager().findFragmentById(R.id.container) != null)
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new VkPosterFragment()).commit();
        else
            getSupportFragmentManager().beginTransaction().add(R.id.container, new VkPosterFragment()).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                showFragment();
            }

            @Override
            public void onError(VKError error) {
                Toast.makeText(VkPosterActivity.this, R.string.error_auth, Toast.LENGTH_LONG).show();
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        //super.onSaveInstanceState(outState, outPersistentState);
    }
}
