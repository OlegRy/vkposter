package com.olegry.vkposter.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.olegry.vkposter.R;
import com.olegry.vkposter.services.VkPosterService;
import com.olegry.vkposter.utils.Utils;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.util.Random;

public class TimeReceiver extends BroadcastReceiver {

    private static final String PREFS_INDEX = "INDEX";
    private static final String POST_INDEX = "POST_INDEX";

    private int mPostIndex;
    private SharedPreferences mPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("tag", "onReceive");
        mPreferences = context.getSharedPreferences(PREFS_INDEX, Context.MODE_PRIVATE);
        mPostIndex = mPreferences.getInt(POST_INDEX, 0);
        String groupId = Utils.GROUP_IDS[mPostIndex];
        performRequest(context, groupId);
        context.startService(new Intent(context, VkPosterService.class));
    }

    private void sendNotification(Context context, String message) {
        NotificationCompat.Builder builder = createBuilder(context, message);
        Notification notification = builder.build();
        getNotificationManager(context).notify(1, notification);
    }

    private NotificationManager getNotificationManager(Context context) {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private NotificationCompat.Builder createBuilder(Context context, String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_keyboard_arrow_right_black_24dp)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);
        return builder;
    }

    private void performRequest(final Context context, String groupId) {
        Random r = new Random();
        int attachmentIndex = r.nextInt(Utils.ATTACHMENTS.length);
        String attachment = Utils.ATTACHMENTS[attachmentIndex];
        String keywords = context.getResources().getString(R.string.keywords);
        String post = context.getResources().getStringArray(R.array.post_messages)[mPostIndex % 4];
        VKRequest request = VKApi.wall().post(VKParameters.from(VKApiConst.OWNER_ID, groupId,
                VKApiConst.MESSAGE, post + "\n" + keywords, VKApiConst.FROM_GROUP, "1",
                VKApiConst.ATTACHMENTS, attachment + "," + Utils.LINK));
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                mPostIndex++;
                if (mPostIndex > 16) mPostIndex = 0;
                mPreferences.edit().putInt(POST_INDEX, mPostIndex).commit();
                if (mPostIndex == 0)
                    sendNotification(context, context.getResources().getString(R.string.content_complete));
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                sendNotification(context, context.getResources().getString(R.string.content_error));
            }
        });
    }


}
