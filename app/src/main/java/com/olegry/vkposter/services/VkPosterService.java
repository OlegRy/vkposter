package com.olegry.vkposter.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import com.olegry.vkposter.fragments.VkPosterFragment;
import com.olegry.vkposter.receivers.TimeReceiver;

public class VkPosterService extends Service {

    private AlarmManager mAlarmManager;
    private long mTimePassed = 180000;
    private TimeReceiver mTimeReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        mTimeReceiver = new TimeReceiver();
        registerReceiver(mTimeReceiver, new IntentFilter(VkPosterFragment.TIME_FILTER));
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notifyIntent = new Intent(VkPosterFragment.TIME_FILTER);
        notifyTimePassed(notifyIntent);
        return super.onStartCommand(intent, flags, startId);
    }

    private void notifyTimePassed(Intent notifyIntent) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, notifyIntent, 0);
        mAlarmManager.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + mTimePassed, pendingIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mTimeReceiver);
    }
}
