package com.olegry.vkposter.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.olegry.vkposter.R;
import com.olegry.vkposter.services.VkPosterService;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class VkPosterFragment extends Fragment {

    public static final String TIME_FILTER = "time_receiver";

    @OnClick(R.id.btn_start_vk_poster)
    void startVkPoster() {
        startVkPosterService();
        showDialog(R.string.start_message);
    }

    @OnClick(R.id.btn_stop_vk_poster)
    void stopVkPoster() {
        getActivity().stopService(new Intent(getActivity(), VkPosterService.class));
        showDialog(R.string.stop_message);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vk_poster, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void startVkPosterService() {
        Intent intent = new Intent(getActivity(), VkPosterService.class);
        getActivity().startService(intent);
    }

    private void showDialog(@StringRes int message) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
        builder.title(R.string.app_name)
                .content(message)
                .positiveText(R.string.ok)
                .cancelable(true);
        builder.build().show();
    }
}
